double g = 2;

class pend{
  float mass;
  float radius;
  float ang;

  PVector anchor;
  double vel;
  double acc = 0;

  pend(float _mass, float _radius, float _ang, PVector _anchor){
    mass = _mass;
    radius = _radius;
    ang = _ang;
    anchor = _anchor;
  }

  pend(float _mass, float _radius, float _ang, PVector _anchor, float _vel, float _acc){
    mass = _mass;
    radius = _radius;
    ang = _ang;
    anchor = _anchor;
    vel = _vel;
    acc = _acc;
  }

  void show(float a, float b){
    float x = a + radius*sin(ang);
    float y = b + radius*cos(ang);
    strokeWeight(4);
    stroke(0);
    line(a,b,x,y);
    ellipse(x,y,mass,mass);
  }

  void show(){
    show(anchor.x, anchor.y);
  }

  PVector getPos(){
    return new PVector(anchor.x + radius*sin(ang), anchor.y + radius*cos(ang));
  }

  void tick(){
    vel += acc;
    ang += vel;
  }

}


class dub{
  pend parent;
  pend child;

  dub(pend _parent, pend _child){
    parent = _parent;
    child = _child;
  }

  void show(){
    PVector pos = parent.getPos();
    parent.show();
    child.show(pos.x,pos.y);
  }

  void tick(){
    double diff = parent.ang - child.ang;
    double mass = 2*parent.mass + child.mass;
    double totMass = parent.mass + child.mass;
    double den = mass - child.mass * Math.cos(2 * diff);

    double theta1 = -g*mass*Math.sin(parent.ang) - child.mass*g*Math.sin(parent.ang - 2*child.ang) - 2*Math.sin(diff)*child.mass*(child.vel*child.vel*child.radius + parent.vel*parent.vel*parent.radius*Math.cos(diff));
    double theta2 = 2*Math.sin(diff)*(parent.vel*parent.vel*parent.radius*(totMass) + g*(totMass)*Math.cos(parent.ang) + child.vel*child.vel*child.radius*child.mass*Math.cos(diff));

    theta1 /= (parent.radius * den);
    theta2 /= (child.radius * den);


    parent.acc = theta1;
    child.acc = theta2;


    parent.tick();
    child.tick();
  }
}
