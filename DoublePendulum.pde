ArrayList<dub> dubs;
int imageX;
int imageY;

int xCount = 25;
int yCount = 25;


void setup(){
  size(600,600);
  dubs = new ArrayList<dub>();

  for(int i = 1; i <= xCount; i++){
    for(int j = 1; j <= yCount; j++){
      pend a = new pend(10,200,i*TWO_PI/xCount, new PVector(width/2,height/2));
      pend b = new pend(10,200,j*TWO_PI/yCount, a.getPos());
      dubs.add(new dub(a,b));
    }
  }

  imageX = width/xCount;
  imageY = height/yCount;
}

void draw(){
  background(124);
  for(int i = 0; i < xCount; i++){
    for(int j = 0; j < yCount; j++){
      int ind = i + j*xCount;
      dub d = dubs.get(ind);
      d.tick();
      pushMatrix();
      scale(1.0/xCount,1.0/yCount);
      translate(i*width,j*height);
      d.show();
      popMatrix();
    }
  }
}
